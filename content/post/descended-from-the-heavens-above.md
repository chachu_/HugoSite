---
title: Descended from the Heavens Above
date: 2021-04-13
hero: "/images/Descended-from-the-Heavens-Above.png"
# excerpt: Guide to emoji usage in Hugo
authors:
  - Urooj Najeeb
#draft: "true"
---

I am left in awe as I close my eyes and internalize the knowledge which is embedded in me since my childhood that the Book we have at home, the Book that we recite and follow, that very same Book has descended from the heavens above.

"Heavens above"

These words keep echoing in my mind, now that I have perhaps a fraction of a droplet of knowledge of the depth of just the first heaven above us: The Observable Universe (as we call it). Every night I dive into the seemingly endless ocean of the universe in front of my computer and take vigorous notes. My pen stopping every now and then by the mind blowing information that my brain is trying to process in its limited human capacity.

And to think that all of that is just the first heaven…Quran came down from 7 heavens above. What a journey that must have been. My heart skips a beat as I try to envision that journey and as I try to understand the glory and status of the heavenly words.

But my feeble mind thinks of this as a complicated and perhaps a lengthy exercise. I am humbled at this point as I am reminded of my limit as a human, and reminded of WHO was sending down this Book.

Allah is not bounded by time, rather time is one of His creation. All He has to say is “Kun” and it is done. The majestic awe of this word gives me goosebumps. How grand is the Owner and the Originator of the Heavens and the Earth