---
title: His Guidance
date: 2021-04-27
hero: "/images/his-guidance.jpg"
# excerpt: Guide to emoji usage in Hugo
authors:
  - Urooj Najeeb
#draft: "true"
---

Guiding others is directly related to the amount of knowledge one has. You can guide someone who's lost his way, facing trouble at work, confused about career paths, and much more such instances.  But our guidance to others is limited and is tied to Allah's will. It is not a free entity of its own. We can only do our best in giving the right guidance, after which the matter is not in our hands anymore.

The guidance which descends from the Heavens above, by our Master, is heavy and enriched with unmeasurable wisdom. It's a crown jewel in our lives. It becomes a lens through which we see light in every situation, changing colors and opening new horizons, somewhat becoming our kaleidoscope.

So, who are the receivers of this majestic guidance?

![The-Book](/images/the-book.jpg)

**Those conscious of Allah**  
The ones who fear Allah, who put Allah before their own desires. These special slaves of the Master of the Universe are jewels, which is why the crown of guidance descends on them.

Once they are blessed with this gift, they restore this in their heart and continuously pray for it to reside there until their last breath. Such souls are free, their spirits always sky high, their faces always glowing, and their company always warm. They are not always rewarded with materialistic satisfactions of this temporary world, rather they are rewarded with something which is priceless in value: peace and a sound heart, yearning to one day meet its Master.