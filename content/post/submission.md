---
title: His Guidance
date: 2021-04-15
hero: "/images/submission-to-allah.png"
#excerpt: Guide to emoji usage in Hugo
authors:
  - Urooj Najeeb
#draft: "true"
---

***So is it other than the religion of Allah they desire, while to Him have submitted [all] those within the heavens and earth, willingly or by compulsion, and to Him they will be returned?***  
Quran, 3:83

As we raise our eyes to the skies above we see massive celestial bodies twinkling down at us from light years away. Some of them known and many of them are unknown, however one thing is for certain that all of these mysterious giants of gas, rocks and ice and all of the even most minute form of living creatures in them are in complete submission to Allah.

They obey His commands in as perfect fashion as can be expected out of them. Yet, when the same Master gave us higher intellect and increased opportunities, we easily are deluded by our wishful thinking, vain desires and the devil's plot.

Are we waiting for the day when the Heavens will be rolled up in scrolls? Will only then our eyes see the light which is now blocked out by the darkness of our own evil deeds.

How beautiful it is to dive deep down in to sincere submission. It is a strange phenomenon, the deeper we dive the higher our souls soar. It is liberating to break the chains of desires that keep us tied only in vain.

Submission today will bring honor tomorrow. Humility today will bring rewards tomorrow. Lets strive to keep our wings humbled in humility so that Allah gives them the honor of flying tomorrow.
