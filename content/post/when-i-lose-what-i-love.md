---
title: When I lose what I love
date: 2021-04-19
hero: "/images/park-bench.jpg"
excerpt: What will bring comfort when we lose something or someone we love? Which words can console us? Where can we find peace to fill the void of what is lost?
authors:
  - Urooj Najeeb
#draft: "true"
---

It is an unbearable emotion that a human feels when they lose something or someone they cherish beyond words. It's a kind of sadness that make us numb, lifeless and hopeless.

Surely divine guidance is needed to calm our trembling heart and restless soul. That guidance is a gift for us, it consoles us. It's parable is non existence in this temporary world, but the closest one that comes to mind is that of a mother pacifying her crying child with love and compassion.

This priceless gift of guidance is the speech from the Lord of the entire Universe and beyond and it is the only channel which reassures us so that we are able to move on. It is the only source which brings light to darkness, hope to misery, and companionship to loneliness.

It is through the heavenly words we have the knowledge that the smallest of atom and the grandest of the celestial bodies, both belong to Allah. Nothing is mine. Nothing is yours. When we lose what we love, a wave of sadness may wash us over, but it shouldn't drown us. His constant nurturing, teaching and reassurance should be like the rope which we can hold on to for survival and growth. "All belongs to Allah" should be so deeply embedded in our hearts that it holds us through any tsunami of grief which life may bring about unexpectedly.

Surely, as long as our hearts are submitted with full devotion to its Master, we will feel the tranquility of guidance descending on it from the Heavens above. This life will become beautiful with a purpose. The light of guidance will radiate far and wide giving hope to others around us who may need it as well. Becoming that beacon of light for others and submitting ourselves as Allah's slaves will undoubtedly bring blessings from the majestic heavens above in ways we can never fathom.