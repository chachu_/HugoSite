---
title: People close to the Master of the Heavens and Earth
date: 2021-05-09
hero: "/images/20.jpg"
excerpt: They have a loving relationship with Allah. Their life is full of gratitude, peace and contentment.
authors:
  - Urooj Najeeb
featured: "true"
---

In Ayah (Verse) 54, of Surah (Chapter) 5 of the Quran, Allah tells us what are the qualities of a nation whom Allah loves:

> They have a loving relationship with Allah. Their life is full of gratitude, peace and contentment.

> They are humble when dealing with other believers.

> They are tough, strong and authoritative with those who are spiteful to the religion of Islam.

> They struggle in Allah's path.

> They are not afraid of criticism that comes their way. Neither are they self righteous, nor do they live by other people's opinions.

This powerful group of traits is what Allah loves. These should be our goals, that we should try to build in ourselves. With each new dawn, we should strive to be the people who earn Allah's love and closeness. That is the ultimate success.